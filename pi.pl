#!/usr/bin/env perl
for ($pi = 1, $s = -1, $i = 3, $p = 1e6, $ppi = 0;
		int($pi * $p) != int($ppi * $p); $i += 2, $s *= -1) {
	($ppi, $pi) = ($pi, $pi + $s * 1.0 / $i);
}
print $pi * 4 . "\n";
