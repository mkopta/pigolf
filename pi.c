#include <stdio.h>
int main(void) {
	long double pi = 1, sign = -1;
	long unsigned i = 3, precision = 1e6, prevpi = 0;
	for (; (long unsigned int) (pi * precision) != prevpi; i += 2, sign *= -1) {
		prevpi = (long unsigned int) (pi * precision);
		pi += sign * 1.0 / i;
	}
	return printf("%Lf\n", pi * 4) > 0;
}
